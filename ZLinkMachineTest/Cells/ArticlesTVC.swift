//
//  ArticlesTVC.swift
//  ZLinkMachineTest
//
//  Created by Angel F Syrus on 06/07/22.
//

import UIKit
import Kingfisher

class ArticlesTVC: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var publishedDateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var articleImage: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!
    
    var articleData : Articles?
    {
        didSet
        {
            self.titleLabel.text = articleData?.title
            self.publishedDateLabel.text = articleData?.publishedAt
            self.descriptionLabel.text = articleData?.description
            self.authorLabel.text = articleData?.author
            self.contentLabel.text = articleData?.content
            let imagelink = articleData?.urlToImage
            if imagelink != nil
            {
                let url = URL(string: imagelink!)
                self.articleImage.kf.indicatorType = .activity
                self.articleImage.kf.setImage(with: url)
                
            }
        }
    }
    
    var storedArticleData : [String : Any]?
    {
        didSet
        {
            self.titleLabel.text = (storedArticleData!["title"] as! String)
            self.publishedDateLabel.text = (storedArticleData!["publishedAt"] as! String)
            self.descriptionLabel.text = (storedArticleData!["description"] as! String)
            self.authorLabel.text = (storedArticleData!["author"] as? String)
            self.contentLabel.text = (storedArticleData!["content"] as! String)
            let imagelink = storedArticleData!["urlToImage"] as! String
            if imagelink != ""
            {
                let url = URL(string: imagelink)
                self.articleImage.kf.indicatorType = .activity
                self.articleImage.kf.setImage(with: url)
                
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
