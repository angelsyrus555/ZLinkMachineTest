//
//  ArticlesListVC.swift
//  ZLinkMachineTest
//
//  Created by Angel F Syrus on 07/07/22.
//

import UIKit
import CoreData

class ArticlesListVC: UIViewController {
    
    @IBOutlet weak var articlesListTable: UITableView!
    @IBOutlet weak var totalResultLabel: UILabel!
    @IBOutlet weak var activityIndictor: UIActivityIndicatorView!
    
    var articlesList = [Articles]()
    var storedArticleList = [[String : Any]]()
    var isAlreadyStored = false
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fetchCoreData()
        
    }
}

extension ArticlesListVC : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isAlreadyStored
        {
            return self.storedArticleList.count
        }
        else
        {
            return self.articlesList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "articlesCell")as! ArticlesTVC
        
        if self.isAlreadyStored
        {
            cell.storedArticleData = self.storedArticleList[indexPath.row]
        }
        else
        {
            if !self.articlesList.isEmpty
            {
                cell.articleData = self.articlesList[indexPath.row]
            }
        }
        
        return cell
    }
    
}


extension ArticlesListVC
{
    func loadArticlesList()
    {
        DispatchQueue.main.async {
            self.activityIndictor.startAnimating()
        }
        var request = URLRequest(url: URL(string: "https://newsapi.org/v2/everything?q=bitcoin&apiKey=fda25d4bcd0949dbb4a45930944c9182&page=1=fda25d4bcd0949dbb4a45930944c9182&page=1")!)
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            DispatchQueue.main.async {
                self.activityIndictor.stopAnimating()
            }
            if let data = data {
                
                let articles = try! JSONDecoder().decode(ArticlesModel.self, from: data)
                self.articlesList = articles.articles!
                DispatchQueue.main.async {
                    self.totalResultLabel.text = "Total result : " + articles.totalResults!.description
                    self.articlesListTable.reloadData()
                    
                    do {
                        let encodedResponse = (try! JSONSerialization.jsonObject(with: JSONEncoder().encode(articles)))
                        let data = try NSKeyedArchiver.archivedData(withRootObject: encodedResponse, requiringSecureCoding: false)
                        self.saveCoreData(data)
                    } catch {
                        NSLog("Unable to archive  \(error)")
                    }
                }
            }
            else
            {
                let alert = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(alertAction)
                self.present(alert, animated: true, completion: nil)
            }
        })
        
        task.resume()
    }
    
    func saveCoreData(_ articleDetails : Data)
    {
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Article", in: context)
        let newArticle = NSManagedObject(entity: entity!, insertInto: context)
        
        newArticle.setValue(articleDetails, forKey: "articlesList")
        
        do {
            try context.save()
            print("Saved to core data")
        } catch {
            print("Failed saving")
        }
    }
    
    func fetchCoreData()
    {
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Article")
        request.returnsObjectsAsFaults = false
        let result = try! context.fetch(request)
        if result.isEmpty
        {
            self.isAlreadyStored = false
            self.loadArticlesList()
        }
        else
        {
            for data in result as! [NSManagedObject] {
                self.isAlreadyStored = true
                let savedData = data.value(forKey: "articlesList")as! Data
                let dataToObj = NSKeyedUnarchiver.unarchiveObject(with: savedData)as? [String : Any]
                let totalResult = (dataToObj!["totalResults"].debugDescription)
                self.totalResultLabel.text = "Total result : " + (totalResult)
                self.storedArticleList = dataToObj!["articles"] as! [[String : Any]]
            }
            
        }
    }
    
}
